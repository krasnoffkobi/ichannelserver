﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class BusinessLogicTest : BusinessLogicBase
    {
        public BusinessLogicTest()
        {
            var options = new DbContextOptionsBuilder<CustomersDBContextTest>()
                .UseInMemoryDatabase(databaseName: "MyDatabase")
                .Options;

            CustomersDBContextTest customersDBContext = new CustomersDBContextTest(options);
            layer = new DatabaseLayer(customersDBContext);
        }
    }
}
