﻿using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace BLL
{
    public class BusinessLogic : BusinessLogicBase
    {
        public BusinessLogic(string connectionString)
        {
            var builder = new DbContextOptionsBuilder<CustomersDBContext>();
            builder.UseSqlServer(connectionString);
            var options = builder.Options;

            CustomersDBContext customersDBContext = new CustomersDBContext(options);
            layer = new DatabaseLayer(customersDBContext);
        }

    }
}
