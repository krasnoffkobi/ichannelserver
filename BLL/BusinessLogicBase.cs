﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;

namespace BLL
{
    public class BusinessLogicBase
    {
        protected DatabaseLayer layer;

        public bool signup(SignupRequest request)
        {
            if (string.IsNullOrEmpty(request.UserName) || string.IsNullOrEmpty(request.FullName) || string.IsNullOrEmpty(request.Password) || string.IsNullOrEmpty(request.ConfirmPassword))
            {
                return false;
            }

            if (request.Password != request.ConfirmPassword)
                return false;

            if (layer.checkIfUsernameExists(request.UserName))
                return layer.signup(request);

            return false;
        }

        public tbl_CustomersList login(LoginRequest request)
        {
            if (string.IsNullOrEmpty(request.UserName) || string.IsNullOrEmpty(request.Password))
            {
                return null;
            }

            return layer.login(request);
        }

        public bool addNewPayment(NewPayment request)
        {
            if (string.IsNullOrEmpty(request.CustomerID) ||
                string.IsNullOrEmpty(request.CardNumber) ||
                string.IsNullOrEmpty(request.CardOwner) ||
                string.IsNullOrEmpty(request.CVV) ||
                !isValidDate(request.ExpirationDate))
            {
                return false;
            }

            return layer.addNewPayment(request);
        }

        public List<tbl_Payments> getPayments(string customerId)
        {
            return layer.getPayments(customerId);
        }

        private bool isValidDate(string str)
        {
            int month = 0;
            int year = 0;
            if (str.Length == 6)
            {
                month = int.Parse(str.Substring(0, 2));
                year = int.Parse(str.Substring(2, 4));
            }
            return isValidDate(1, month, year);
        }

        private bool isValidDate(int d, int m, int y)
        {
            m = m - 1;
            return m >= 0 && m < 12 && d > 0 && d <= this.daysInMonth(m, y);
        }

        private int daysInMonth(int m, int y)
        {
            switch (m)
            {
                case 1:
                    return ((y % 4 == 0) && (y % 100 == 0)) || y % 400 == 0 ? 29 : 28;
                case 8:
                case 3:
                case 5:
                case 10:
                    return 30;
                default:
                    return 31;
            }
        }
    }
}

