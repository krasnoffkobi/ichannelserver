﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace iChannelServerProject.Controllers
{
    [Produces("application/json")]
    [Route("api/Signup")]
    public class SignupController : Controller
    {
        private BusinessLogicBase businessLogic;
        private IConfiguration _configuration;

        public SignupController(IConfiguration Configuration)
        {
            _configuration = Configuration;

            if (_configuration.GetConnectionString("IChannelDatabase") != null)
                businessLogic = new BusinessLogic(_configuration.GetConnectionString("IChannelDatabase"));
            else
                businessLogic = new BusinessLogicTest();
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody] SignupRequest request)
        {
            if (businessLogic.signup(request))
                return Ok("ok");
            else
                return BadRequest("Error: Check validation of the form");
        }
    }

    
}