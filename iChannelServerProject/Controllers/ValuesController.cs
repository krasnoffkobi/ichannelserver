﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace iChannelServerProject.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        #region sample methods
        private BusinessLogicBase businessLogic;
        private IConfiguration _configuration;

        public ValuesController(IConfiguration Configuration)
        {
            _configuration = Configuration;

            if (_configuration.GetConnectionString("IChannelDatabase") != null)
                businessLogic = new BusinessLogic(_configuration.GetConnectionString("IChannelDatabase"));
            else
                businessLogic = new BusinessLogicTest();
        }

        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            string CustomerID = "";
            if (User != null)
                CustomerID = (User.Claims.Where(x => x.Type.ToString().IndexOf("sid") > 0).ToList()[0].Value).ToString();
            else
                CustomerID = "64A8D44A-91BB-4AE7-BE42-66D93A9E0EEC";

            List<tbl_Payments> res = businessLogic.getPayments(CustomerID);
            if (res.Count > 0)
                return Ok(res);
            else
                return Ok("[]");
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]NewPayment request)
        {
            if (User != null)
                request.CustomerID = (User.Claims.Where(x => x.Type.ToString().IndexOf("sid") > 0).ToList()[0].Value).ToString();

            if (businessLogic.addNewPayment(request))
                return Ok("[]");
            else
                return BadRequest("Error: Check validation of the form");
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        #endregion


    }
}
