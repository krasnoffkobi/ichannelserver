﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BLL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace iChannelServerProject.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {
        private BusinessLogicBase businessLogic;
        private IConfiguration _configuration;

        public LoginController(IConfiguration Configuration)
        {
            _configuration = Configuration;

            if (_configuration.GetConnectionString("IChannelDatabase") != null)
                businessLogic = new BusinessLogic(_configuration.GetConnectionString("IChannelDatabase"));
            else
                businessLogic = new BusinessLogicTest();
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody] LoginRequest request)
        {
            var res = businessLogic.login(request);

            if (res != null)
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Sid, res.Id.ToString())
                };

                SymmetricSecurityKey key;

                if (_configuration != null)
                    key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));
                else
                    key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("dd%88*377f6d&f£$$£$FdddFF33fssDG^!3"));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "yourdomain.com",
                    audience: "yourdomain.com",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: creds);

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    fullName = res.FullName
                });
            }
            else
                return BadRequest("Error: Check validation of the form");
        }


    }


}