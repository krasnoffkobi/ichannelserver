using DAL.Models;
using iChannelServerProject.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class SignupLoginTest
    {
        private readonly SignupController _signupController;
        private readonly LoginController _loginController;
        private readonly ValuesController _valuesController;

        private string uid = "";

        public SignupLoginTest()
        {
            _signupController = new SignupController(null);
            _loginController = new LoginController(null);
            _valuesController = new ValuesController(null);
        }

        [TestInitialize]
        public void TestSetup()
        {
            
        }

        [TestMethod]
        public void SignupTest()
        {
            SignupRequest request = new SignupRequest();
            request.FullName = "kobi krasnoff";
            request.UserName = "krasnoff2";
            request.Password = "1111";
            request.ConfirmPassword = "1111";

            var res = _signupController.Post(request);
            var okResult = res as OkResult;
            var badRequestResult = res as BadRequestObjectResult;

            if (okResult != null)
                Assert.IsTrue(true, "Signup Suucess");
            else if (badRequestResult != null)
                Assert.IsFalse(true, "Signup Failed: " + badRequestResult.Value);
        }

        [TestMethod]
        public void LoginTest()
        {
            LoginRequest request = new LoginRequest();
            request.UserName = "krasnoff2";
            request.Password = "1111";
            
            var res = _loginController.Post(request);
            var okResult = res as OkObjectResult;
            var badRequestResult = res as BadRequestObjectResult;

            if (okResult != null)
            {
                //uid = res.okResult
                Assert.IsTrue(true, "Login Suucess", okResult.Value);
            }
            else if (badRequestResult != null)
                Assert.IsFalse(true, "Login Failed: " + badRequestResult.Value);
        }

        [TestMethod]
        public void NewPayment()
        {
            NewPayment request = new NewPayment();
            request.CustomerID = "64A8D44A-91BB-4AE7-BE42-66D93A9E0EEC";
	        request.CardNumber = "45804580458045804580";
            request.CardOwner = "Krasnoff Kobi";
            request.CVV = "123";
            request.ExpirationDate = "201808";
            request.Amount = 231.1;

            var res = _valuesController.Post(request);
            var okResult = res as OkResult;
            var badRequestResult = res as BadRequestObjectResult;

            if (okResult != null)
                Assert.IsTrue(true, "Login Suucess");
            else if (badRequestResult != null)
                Assert.IsFalse(true, "Login Failed: " + badRequestResult.Value);
        }

        [TestMethod]
        public void getPayments()
        {
            var res = _valuesController.Get();
            var okResult = res as OkObjectResult;
            var badRequestResult = res as BadRequestObjectResult;

            if (okResult != null)
                Assert.IsTrue(true, "Login Suucess");
            else if (badRequestResult != null)
                Assert.IsFalse(true, "Login Failed: " + badRequestResult.Value);
        }

        [TestCleanup]
        public void testClean()
        {
            
        }
    }
}
