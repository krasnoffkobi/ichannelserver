﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Models
{
    public class DatabaseLayer
    {
        public ICustomersDBContext context;
        
        public DatabaseLayer(ICustomersDBContext context)
        {
            this.context = context;
        }

        public bool checkIfUsernameExists(string userName)
        {
            bool res = true;

            try
            {
                // check for duplication
                if (context.Tbl_CustomersList.Where(s => s.UserName == userName).ToList().Count > 0)
                {
                    res = false;
                }
            }
            catch (Exception ex)

            {
                res = false;
            }

            return res;
        }

        public bool signup(SignupRequest request)
        {
            bool validationsuccess = true;

            try
            {
                tbl_CustomersList newRec = new tbl_CustomersList();

                newRec.Id = Guid.NewGuid();
                newRec.CreateDate = DateTime.Now;
                newRec.FullName = request.FullName;
                newRec.UserName = request.UserName;
                newRec.Password = request.Password;

                context.Tbl_CustomersList.Add(newRec);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                validationsuccess = false;
            }

            return validationsuccess;
        }

        public tbl_CustomersList login(LoginRequest request)
        {
            tbl_CustomersList validationsuccess;
            
            try
            {
                using (context)
                {
                    var res = context.Tbl_CustomersList.Where(s => s.UserName == request.UserName && s.Password == request.Password).ToList();

                    if (res.Count == 0)
                    {
                        validationsuccess = null;
                    }
                    else
                    {
                        validationsuccess = res[0];
                    }
                }
            }
            catch (Exception ex)
            {
                validationsuccess = null;
            }

            return validationsuccess;
        }

        public bool addNewPayment(NewPayment request)
        {
            bool validationsuccess = true;

            try
            {
                tbl_Payments newRec = new tbl_Payments();

                newRec.TransactionID = Guid.NewGuid();
                newRec.CreateDate = DateTime.Now;
                newRec.CustomerID = request.CustomerID;
                newRec.CardNumber = request.CardNumber;
                newRec.CardOwner = request.CardOwner;
                newRec.CVV = request.CVV;
                newRec.ExpirationDate = new DateTime(int.Parse(request.ExpirationDate.Substring(2, 4)), int.Parse(request.ExpirationDate.Substring(0, 2)), 1);
                newRec.Amount = request.Amount;

                context.Tbl_Payments.Add(newRec);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                validationsuccess = false;
            }

            return validationsuccess;
        }

        public List<tbl_Payments> getPayments(string customerId)
        {
            List<tbl_Payments> res = new List<tbl_Payments>();

            try
            {
                res = context.Tbl_Payments.Where(s => s.CustomerID == customerId).ToList();

                foreach(tbl_Payments el in res)
                {
                    el.CardNumber = el.CardNumber.Substring(el.CardNumber.Length - 4);
                }
            }
            catch (Exception ex)
            {
                res = null;
            }

            return res;
        }
    }

    public class NewPayment
    {
        public string CustomerID { get; set; }
        public string CardNumber { get; set; }
        public string CardOwner { get; set; }
        public string CVV { get; set; }
        public string ExpirationDate { get; set; }
        public double Amount { get; set; }
    }

    public class SignupRequest
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class LoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
