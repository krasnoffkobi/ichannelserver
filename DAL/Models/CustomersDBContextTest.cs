﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class CustomersDBContextTest : DbContext, ICustomersDBContext
    {
        public CustomersDBContextTest(DbContextOptions<CustomersDBContextTest> options) : base(options)
        {

        }

        public DbSet<tbl_CustomersList> Tbl_CustomersList { get; set; }
        public DbSet<tbl_Payments> Tbl_Payments { get; set; }

    }
}
