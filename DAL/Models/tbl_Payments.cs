﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class tbl_Payments
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid TransactionID { get; set; }
        public string CustomerID { get; set; }
        public DateTime CreateDate { get; set; }
        public string CardNumber { get; set; }
        public string CardOwner { get; set; }
        public string CVV { get; set; }
        public DateTime ExpirationDate { get; set; }
        public double Amount { get; set; }
    }
}