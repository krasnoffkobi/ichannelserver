﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public interface ICustomersDBContext : IDisposable
    {
        DbSet<tbl_CustomersList> Tbl_CustomersList { get; set; }
        DbSet<tbl_Payments> Tbl_Payments { get; set; }
        int SaveChanges();
    }
}
