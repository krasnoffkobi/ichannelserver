﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class CustomersDBContext : DbContext, ICustomersDBContext
    {
        public CustomersDBContext(DbContextOptions<CustomersDBContext> options) : base(options)
        {

        }

        public DbSet<tbl_CustomersList> Tbl_CustomersList { get; set; }
        public DbSet<tbl_Payments> Tbl_Payments { get; set; }

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=IChannelDB;integrated security=True;");
        }*/
    }
}
